/*
 * Very simple prime sieve (Eratosthenes) in c.
 * kjalaric@gitlab
 */

#include <stdio.h>

#define MAX_NUM 500

typedef unsigned int uint;

void printArrayOfUint(uint* array, uint size) {
   for (uint i = 0; i < size; i++) {
      printf("%d\n", array[i]);
   }
   return;
}

int main() {
   uint primes[MAX_NUM-2];
   uint s1[MAX_NUM-2];
   uint s2[MAX_NUM-2];
   uint primes_size = 0;
   uint s1_size = MAX_NUM;  // we fill this in before starting
   uint s2_size = 0;

   // initialise s1 with all numbers in [2, MAX_NUM) to avoid division by zero or one
   for (uint i = 2; i < MAX_NUM-2; i++) {
      s1[i-2] = i;
   }

   // 'sieve' between s1 and s2, removing non-primes
   uint newprime;
   while (s1_size > 0) {
      // add first value of s1 to primes list
      newprime = s1[0];
      primes[primes_size] = newprime;
      primes_size++;

      // sieve s1 into s2
      s2_size = 0;
      for (uint i = 0; i < s1_size; i++) {
         if (s1[i] % newprime) {
            s2[s2_size] = s1[i];
            s2_size++;
         }
      }

      // if no new elements were sieved into s2, have finished
      if (!s2_size) {
         printArrayOfUint(primes, primes_size);
         return 0;
      }

      // add first value of s2 to primes list
      newprime = s2[0];
      primes[primes_size] = newprime;
      primes_size++;

      // sieve s2 into s1
      s1_size = 0;
      for (uint i = 0; i < s2_size; i++) {
         if (s2[i] % newprime) {
            s1[s1_size] = s2[i];
            s1_size++;
         }
      }

      // if no new elements were sieved into s1, have finished
      if (!s1_size) {
         printArrayOfUint(primes, primes_size);
         return 0;
      }
   }
}
