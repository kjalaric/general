'''
Simple (horrendously inefficient) but elegant implementation of Sieve of Eratosthenes
kjalaric@gitlab
'''

max = 500
primes = list()
nums = range(2, max)
while (len(nums)):
    primes.append(nums[0])
    nums = [x for x in nums if (x % primes[-1])]
print (primes)
