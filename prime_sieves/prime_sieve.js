/*
 * Sieve of Eratosthenes in javascript, without pre-allocation of arrays
 * kjalaric@gitlab
 */

var MAX_NUM = 500

var primes = []
var s1 = []
var s2 = []

for (i = 2; i < MAX_NUM; i++) {
   s1.push(i)
}

// sieve between s1 and s2, removing non-primes
var newprime
while(s1.length) {
   // sieve s1 into s2
   newprime = s1[0]
   primes.push(newprime)
   s2 = []
   for (i = 0; i < s1.length; i++) {
      if (s1[i] % newprime) {
         s2.push(s1[i])
      }
   }

   if (!s2.length) {
      console.log(primes)
      return 0
   }

   // sieve s2 into s1
   newprime = s2[0]
   primes.push(newprime)
   s1 = []

   for (i = 0; i < s2.length; i++) {
      if (s2[i] % newprime) {
         s1.push(s2[i])
      }
   }

   if (!s1.length) {
      console.log(primes)
      return 0
   }
}
