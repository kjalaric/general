/*
 * (relatively) efficient prime sieve
 * kjalaric@gitlab
 */

#include <vector>
#include <cmath>
#include <iostream>
#include <chrono>
#include <ctime>
#include <cstdint>

// time benchmarks for max = 1million (i7-7700K @ 4.20GHz with 8M cache, 16GB ram, Windows 10 64bit):
// uint32_t, uint_fast32_t: 6.7s
// uint64_t, uint_fast64_t: 15.47s
// (this implementation won't work with anything too big due to an inability to allocate enough contiguous ram for std::vector)
typedef uint32_t uint;

static uint expected_max_num_primes(uint max) {
	return static_cast<uint>(0.245743*pow(max, 0.917376)+50); // expect roughly this many primes for a given max (empirical coeficients)
}

static void vectorImplementation(uint max) {
	std::vector<uint> primes;
	std::vector<uint> nums1;
	std::vector<uint> nums2;
	primes.reserve(expected_max_num_primes(max));
	nums1.reserve(max - 2);  // range is [2, max]
	nums2.reserve(max - 2);  // range is [2, max]

	// initialise nums1 with all possible numbers between 2 (first prime) and max
	for (uint i = 2; i <= max; i++) {
		nums1.push_back(i);
	}

	// 'sieve' numbers between two vectors, removing composites in the process
	uint newprime;
	while (nums1.size() > 0) {
		newprime = nums1[0];
		primes.push_back(newprime);

		for (uint i : nums1) {
			if (i % newprime) {
				nums2.push_back(i);
			}
		}

		if (!nums2.size()) break;
		nums1.clear();
		newprime = nums2[0];
		primes.push_back(newprime);

		for (uint i : nums2) {
			if (i % newprime) {
				nums1.push_back(i);
			}
		}
		nums2.clear();
	}

	std::cout << "Vector last prime: " << primes.back() << " (" << primes.size() << " elements, " << primes.capacity() << " allocated)" << std::endl;

	return;
}


int main() {
	uint max = 1000000;  // highest tested so far is 80000000; cancelled execution after 10min
	std::cout << "Expected upper limit of primes for " << max << ": " << expected_max_num_primes(max) << std::endl;

	auto v_start = std::chrono::high_resolution_clock::now();
	vectorImplementation(max);
	auto v_end = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double> v_time = std::chrono::duration_cast<std::chrono::milliseconds>(v_end - v_start);
	std::cout << "Vector: " << v_time.count();

	return 0;
}
