// Sieve of Eratosthenes in Go
package main

import (
    "fmt"
)

const MAX_NUM = 500


func main() {
    primes := [MAX_NUM-2]uint{}
    s1 := [MAX_NUM-2]uint{}
    s2 := [MAX_NUM-2]uint{}

    var primes_size uint = 0
    var s1_size uint = MAX_NUM-2
    var s2_size uint = 0

    // initialise s1 with all numbers in [2, MAX_NUM) to avoid division by zero or one
    for i:= uint(2); i < MAX_NUM-2; i++ {
        s1[i-2] = i
    }

    // 'sieve' between s1 and s2, removing non-primes
    var newprime uint = 0
    for {
      // add first value of s1 to primes list
      newprime = s1[0];
      primes[primes_size] = newprime
      primes_size++

      // sieve s1 into s2
      s2_size = 0;
      for i:= uint(0); i < s1_size; i++ {
         if (s1[i] % newprime != 0) {
            s2[s2_size] = s1[i]
            s2_size++
         }
      }

      // if no new elements were sieved into s2, have finished
      if (s2_size == 0) {
         fmt.Printf("%d", primes[0:primes_size])
         break
      }

      // add first value of s2 to primes list
      newprime = s2[0];
      primes[primes_size] = newprime;
      primes_size++

      // sieve s2 into s1
      s1_size = 0;
      for i:= uint(0); i < s2_size; i++ {
         if (s2[i] % newprime != 0) {
            s1[s1_size] = s2[i]
            s1_size++
         }
      }

      // if no new elements were sieved into s1, have finished
      if (s1_size == 0) {
         fmt.Printf("%d", primes[0:primes_size])
         break
      }

    }
}
