'''
Concatenates all files in a local 'to_concatenate' directory into a single 'concatenated.x' file, where x is
the common file extension (or 'out' if they differ). 

kjalaric@gitlab
'''

import os

DIRECTORY = "to_concatenate/"
OUTPUT_FILE = "concatenated.{}"

if __name__ == "__main__":
    files_to_concatenate = os.listdir(DIRECTORY)
    
    files_are_all_the_same = True
    check_file_extension = lambda a : (a.split(".")[-1] == files_to_concatenate[0].split(".")[-1]) and files_are_all_the_same
    
    for x in files_to_concatenate[1:]:
        files_are_all_the_same = check_file_extension(x)
        
    if files_are_all_the_same:
        OUTPUT_FILE = OUTPUT_FILE.format(files_to_concatenate[0].split(".")[-1])
    else:
        OUTPUT_FILE = OUTPUT_FILE.format("out")
    
    
    with open(OUTPUT_FILE, "w") as fo:
        for filename in files_to_concatenate:
            

            print("Concatenating {}...".format(filename))
            
            with open(DIRECTORY + filename, "r") as fi:
                fo.write(fi.read())
                
            fo.write("\n")
            
    if not files_are_all_the_same:
        print("FYI: File extensions aren't all the same.")