'''
Short script to change file extension of all files in a subdirectory.

kjalaric@gitlab
'''

import os

DIRECTORY_OF_INTEREST = "insertion_sequence_searches/"
NEW_FILE_EXTENSION = "html"

if __name__ == "__main__":
    for filename in os.listdir(DIRECTORY_OF_INTEREST):
        os.rename(DIRECTORY_OF_INTEREST + filename.split(".")[0], DIRECTORY_OF_INTEREST + filename.split(".")[0] + "." + NEW_FILE_EXTENSION)