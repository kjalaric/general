'''
Check to see if a number is sphenic.

Based on responses from:
https://codegolf.stackexchange.com/questions/135193/is-it-a-sphenic-number
'''
from math import fmod

def is_sphenic(number):
    div = 0
    for l in range(2, number):
        if fmod(float(number)/l, l):
            if not (number % l):
                number /= l
                div += 1
        else:
            return False
    return(div == 3)

                
if __name__ == "__main__":
    for i in range(500):
        if is_sphenic(i):
            print(i)