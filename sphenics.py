'''
Script to produce sphenic numbers using the Sieve of Eratosthenes and an
algorithm of my design (which may also exist elsewhere).

kjalaric@gitlab
'''

max_prime = 100
primes = list()
nums = range(2, max_prime)
while (len(nums)):
    primes.append(nums[0])
    nums = [x for x in nums if (x % primes[-1])]
    
sphenics = set()
for index_1 in range(0, len(primes)-2):
    for index_2 in range(index_1+1, len(primes)-1):
        for index_3 in range(index_2+1, len(primes)):
            sphenics.add(primes[index_1]*primes[index_2]*primes[index_3])